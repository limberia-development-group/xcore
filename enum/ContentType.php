<?php


namespace enum;


/**
 * Class ContentType
 * @package enum
 */
class ContentType
{

    public const TEXT   = 'text/plain';
    public const JSON   = 'application/json';
    public const BINARY = 'application/octet-stream';
}