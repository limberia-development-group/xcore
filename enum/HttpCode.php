<?php


namespace enum;


/**
 * Class HttpCode
 * @package enum
 */
class HttpCode
{

    public const MESSAGE = [
        200 => 'OK',// don't remove this value!
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Not Allowed',//для HTTP-методов, которые не поддерживаются API
        500 => 'Internal server error',// don't remove this value!
        507 => 'Storage overload',
        524 => 'Timeout overload'
    ];
}