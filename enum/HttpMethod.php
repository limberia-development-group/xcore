<?php


namespace enum;


/**
 * Class EHttpMethodsList
 * @package enum
 *
 * Now is not allowed:
 * COPY, HEAD, OPTIONS, LINK, UNLINK, PURGE, LOCK, UNLOCK, PROPFIND, VIEW
 * (see Postman)
 * may be i add their later..
 */
class HttpMethod
{

    public const GET    = 'GET';
    public const POST   = 'POST';
    public const PATCH  = 'PATCH';
    public const PUT    = 'PUT';
    public const DELETE = 'DELETE';
}