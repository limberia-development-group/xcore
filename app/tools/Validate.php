<?php


namespace kernel\tools;


use enum\ContentType;
use ReflectionClass;

/**
 * Class Validate
 * @package kernel\tools
 */
class Validate
{

    public static function isContentTypeValid(string $contentType): bool
    {
        $allowedTypes = (new ReflectionClass(new ContentType()))->getConstants();
        foreach ($allowedTypes as $key => $value) {
            if ($contentType === $key) {
                return true;
            }
        }
        return false;
    }

    public static function isGUIDValid(string $guid): bool
    {
        if (strlen($guid)) {
            $parts = explode('-', $guid);
            // validate gid parts length
            if (
                (count($parts) === 5) &&
                (strlen($parts[0]) === 8) &&
                (strlen($parts[1]) === 4) &&
                (strlen($parts[2]) === 4) &&
                (strlen($parts[3]) === 4) &&
                (strlen($parts[4]) === 12)
            ) {
                return true;
            }
        }
        return false;
    }
}