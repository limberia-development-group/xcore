<?php


namespace kernel\tools;


/**
 * Class Convert
 * @package kernel\tools
 */
class Convert
{

    public static function AnyToJson(mixed $data): string
    {
        try {
            $output = json_encode($data, JSON_THROW_ON_ERROR|JSON_PRETTY_PRINT);
            if ($output === false) {
                throw new \Exception('Unable to convert data to JSON', 500);
            }
            return $output;
        }
        catch (\Exception | \JsonException $exception) {
            //(new HttpResponse($error->getMessage(), $error->getCode(), false, false))->Run();
        }
    }

    public static function GetHash(string $stringOrFilename, bool $isFile = false): string {
        return ($isFile === true)
            ? sha1_file($stringOrFilename)
            : sha1($stringOrFilename);
    }
}