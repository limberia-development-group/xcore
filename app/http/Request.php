<?php


namespace kernel\http;


use kernel\http\Response;
use app\Settings;


/**
 * Class Request
 * @package kernel\http
 *
 * Класс отвечает за запросы, поступающие в API извне.
 */
class Request
{

    private static string $requestId;

    public function __construct()
    {
        // test response for debug
        (new Response((object)[
            'argv'         => $_SERVER['argv'][0] ?? 'no params',
            'argc'         => $_SERVER['argc'],
            'http method'  => $_SERVER['REQUEST_METHOD'],
            'input'        => file_get_contents('php://input'),
            'content type' => $_SERVER['CONTENT_TYPE'],
            'http auth'    => $_SERVER['HTTP_AUTHORIZATION'],
            'port'         => $_SERVER['SERVER_PORT'],
        ], 200, Settings::GUID_PLACEHOLDER, false, true))->Send();
    }

}