<?php


namespace kernel\http;


use app\Settings;
use enum\ContentType;
use enum\HttpCode;
use JetBrains\PhpStorm\NoReturn;
use kernel\tools\Convert;

/**
 * Class Response
 * @package kernel\http
 *
 * Класс отвечает за формирование ответов на запросы к API.
 */
class Response
{

    private const DEFAULT_RESPONSE_STATE = true;// success by default (code 200 OK)

    private string $requestGuid;       // GUID, поступивший от клиентского приложения
    private bool   $isFile;            // флаг, показывающий, является ли ответ API файлом
    private int    $httpCode;          // HTTP-код ответа
    private array  $httpHeaders;       // HTTP-заголовки ответа
    private string $httpContentType;   // тип данных для создания HTTP-заголовка об этом
    private bool   $isSuccessResponse; // флаг, определяющий внутри API, является ли ответ успешным
    private string $responseBody;      // тело ответа
    private string $responseBodyType;  // формат тела ответа

    public function __construct(
        string|object $responseBody,
        int $httpCode,
        string $guid,
        bool $isFile = false,
        bool $isSuccessResponse = self::DEFAULT_RESPONSE_STATE
    ) {
        /**
         * !! Заголовки формируются ТОЛЬКО здесь. API не важно, какими они будут - HttpResponse сам определяет какой набор заголовков отправить клиенту в ответе.
         * 2. Тело ответа
         */
        $this
            ->setFieldIsSuccessResponse($isSuccessResponse)
            ->setFieldIsFile($isFile)
            ->setFieldResponseBodyType($responseBody)
            ->determineContentType($this->getFieldResponseBodyType())
            ->setFieldResponseBody($responseBody)
            ->setFieldHttpCode($httpCode)
            ->setFieldRequestGuid($guid)
            ->setDefaultHttpHeaders();
    }

    #[NoReturn]
    final public function Send(): void {
        foreach ($this->getFieldHttpHeaders() as $currentHeader) {
            header($currentHeader);
        }
        print $this->getFieldResponseBody();
        die();
    }



    private function determineContentType(string $dataType): Response {
        $this->setFieldHttpContentType(match ($dataType) {
            'string' => ($this->getFieldIsFile() === true)
                ? ContentType::BINARY
                : ContentType::TEXT,
            'object' => ContentType::JSON,
            default => ContentType::TEXT
        });
        return $this;
    }

    // TODO упростить логику работы с заголовками
    // TODO вынести работу с заголовками в отдельный класс и использовать их как объекты

    private function addHttpHeader(string $httpHeader): void {
        //TODO modify headers with key-value for update custom headers such as "Content-Type" = "application/json" etc.
        if (!in_array($httpHeader, $this->getFieldHttpHeaders(), true)) {
            $this->httpHeaders[] = $httpHeader;
        }
    }

    private function resetHttpHeaders(): Response {
        unset($this->httpHeaders);
        $this->httpHeaders = [];
        return $this;
    }

    private function setMainHttpHeaderByHttpCode(): void {
        $isCodeKnown = false;
        foreach (HttpCode::MESSAGE as $code => $message) {
            if ($this->getFieldHttpCode() === $code) {
                $isCodeKnown = true;
                break;
            }
        }
        if ($isCodeKnown === true) {
            $codeValue = $this->getFieldHttpCode();
        } else {
            $codeValue = ($this->getFieldIsSuccessResponse() === true)
                ? Settings::HTTP_CODE_DEFAULT_SUCCESS
                : Settings::HTTP_CODE_DEFAULT_ERROR;
        }
        $this->addHttpHeader(sprintf(
            '%s %d %s',
            Settings::HTTP_VERSION_USED,
            $codeValue,
            HttpCode::MESSAGE[$codeValue]
        ));
    }

    private function setDefaultHttpHeaders(): void {
        $headers = [
            'RequestID' => $this->getFieldRequestGuid(),
            'Content-Type' => $this->getFieldHttpContentType(),
            'Body-Hash' => Convert::GetHash($this->getFieldResponseBody())
        ];
        $this
            ->resetHttpHeaders()
            ->setMainHttpHeaderByHttpCode();
        foreach ($headers as $headerTitle => $headerValue) {
            $this->addHttpHeader(sprintf('%s: %s', $headerTitle, $headerValue));
        }
    }



    private function setFieldRequestGuid(string $guid): Response {
        //$this->requestGuid = Request::GetGUID();
        $this->requestGuid = $guid;
        return $this;
    }

    private function getFieldRequestGuid(): string {
        return (is_string($this->requestGuid))
            ? $this->requestGuid
            : Settings::GUID_PLACEHOLDER;
    }

    private function setFieldIsFile(bool $isFile): Response {
        $this->isFile = $isFile;
        return $this;
    }

    private function getFieldIsFile(): bool {
        return (is_bool($this->isFile))
            ? $this->isFile
            : false;// use TEXT|JSON by default
    }

    private function setFieldHttpCode(int $httpCode): Response {
        $this->httpCode = $httpCode;
        return $this;
    }

    private function getFieldHttpCode(): int {
        return (is_int($this->httpCode))
            ? $this->httpCode
            : Settings::HTTP_CODE_DEFAULT_ERROR;
    }

    private function setFieldHttpHeaders(array $httpHeaders): Response {
        $this->httpHeaders = $httpHeaders;
        return $this;
    }

    private function getFieldHttpHeaders(): array {
        return (is_array($this->httpHeaders))
            ? $this->httpHeaders
            : [];
    }

    private function setFieldHttpContentType(string $httpContentType): Response {
        $this->httpContentType = $httpContentType;
        return $this;
    }

    private function getFieldHttpContentType(): string {
        return (is_string($this->httpContentType) && !empty($this->httpContentType))
            ? $this->httpContentType
            : ContentType::TEXT;
    }

    private function setFieldIsSuccessResponse(bool $isSuccessResponse): Response {
        $this->isSuccessResponse = $isSuccessResponse;
        return $this;
    }

    private function getFieldIsSuccessResponse(): bool {
        return (is_bool($this->isSuccessResponse))
            ? $this->isSuccessResponse
            : self::DEFAULT_RESPONSE_STATE;
    }

    private function setFieldResponseBody(string|object $responseBody): Response {
        $this->responseBody = ($this->getFieldHttpContentType() === ContentType::JSON)
            ? Convert::AnyToJson($responseBody)
            : (string)$responseBody;
        return $this;
    }

    private function getFieldResponseBody(): string {
        return (is_string($this->responseBody))
            ? $this->responseBody
            : '';
    }

    private function setFieldResponseBodyType(string|object $responseBody): Response {
        $this->responseBodyType = gettype($responseBody);
        return $this;
    }

    private function getFieldResponseBodyType(): string {
        if (!is_string($this->responseBodyType) || empty($this->responseBodyType)) {
            (new Response("Unknown response body type", 500, $this->getFieldRequestGuid(), false, false))->Send();
        }
        return $this->responseBodyType;
    }
}