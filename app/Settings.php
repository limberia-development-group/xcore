<?php


namespace app;


use enum\ContentType;
use enum\HttpMethod;

class Settings
{
    public const HTTP_VERSION_USED         = 'HTTP/1.1';// TODO change to HTTP/2
    public const HTTP_PORT_DEFAULT         = 80;// TODO change to 443 in production
    public const HTTP_PORT_UNKNOWN         = -1;
    public const ARGC_MAX_VALUES           = 1;
    public const ARGC_UNKNOWN_VALUE        = -1;
    public const HTTP_ALLOWED_METHODS      = [
        HttpMethod::GET,
        HttpMethod::POST,
        HttpMethod::PUT,
        HttpMethod::PATCH,
        HttpMethod::DELETE
    ];
    public const HTTP_NOBODY_METHODS       = [
        HttpMethod::GET
    ];
    public const ALLOWED_CONTENT_TYPES     = [
        ContentType::JSON,
        ContentType::BINARY
    ];
    public const HTTP_CODE_DEFAULT_ERROR   = 500;
    public const HTTP_CODE_DEFAULT_SUCCESS = 200;
    public const GUID_PLACEHOLDER          = '00000000-0000-0000-0000-000000000000';
}