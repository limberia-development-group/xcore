<?php


namespace devtools;


use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Logger
{

    private const DEFAULT_FILENAME = '__log';
    private const DELIMITER_SYMBOL = '-';
    private const DELIMITER_REPEAT = 120;

    private string $filename          = self::DEFAULT_FILENAME;
    private bool   $isDelimiterNeedle = false;
    private mixed  $logData;

    public function __construct(mixed $logData) {
        $this->setFieldLogData($logData);
        $this->setFieldFilename(self::DEFAULT_FILENAME);
    }

    #[Pure] #[ArrayShape(['message' => "string", 'code' => "int|mixed", 'file' => "string", 'line' => "int", 'stackTrace' => "array"])]
    public static function FormatException(Exception $exception): array {
        return [
            'message'    => $exception->getMessage() ?? 'EXCEPTION OCCURRED',
            'code'       => $exception->getCode() ?? 0,
            'file'       => $exception->getFile() ?? 'unknown file',
            'line'       => $exception->getLine() ?? 0,
            'stackTrace' => $exception->getTrace() ?? []
        ];
    }

    final public function SetFilename(string $filename): Logger
    {
        $this->setFieldFilename(
            (trim($filename) !== '')
                ? trim($filename)
                : self::DEFAULT_FILENAME
        );
        return $this;
    }

    final public function Write(bool $isDelimiterNeedle = false): Logger
    {
        $this->setFieldIsDelimiterNeedle($isDelimiterNeedle);
        print $this->getFormattedRaw();
        return $this;
    }

    final public function SaveRaw(bool $isDelimiterNeedle = false): void {
        $this->setFieldIsDelimiterNeedle($isDelimiterNeedle);
        $this->saveLog($this->getFormattedRaw());
    }

    final public function SaveJson(): void {
        $this->saveLog(sprintf(
            '%s%s',
            json_encode($this->getFieldLogData(), JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES),
            PHP_EOL
        ), true);
    }

    private function getDelimiter(): string {
        $string = '';
        $counter = 0;
        while ($counter < self::DELIMITER_REPEAT) {
            $string .= self::DELIMITER_SYMBOL;
            $counter++;
        }
        unset($counter);
        return $string;
    }

    private function getFormattedRaw(): string {
        return sprintf(
            '%s%s%s%s%s',
            print_r($this->getFieldLogData(), true),
            PHP_EOL,
            PHP_EOL,
            ($this->getFieldIsDelimiterNeedle() === true)
                ? $this->getDelimiter()
                : '',
            PHP_EOL
        );
    }

    private function saveLog(mixed $logData, bool $isJson = false): void {
        $extension = ($isJson === true)
            ? '.json'
            : '.txt';
        file_put_contents(
            $this->getFieldFilename() . $extension,
            $logData,
            FILE_APPEND
        );
    }

    private function setFieldFilename(string $filename): void {
        $this->filename = $filename;
    }
    private function getFieldFilename(): string {
        return (!is_null($this->filename)) ? $this->filename : self::DEFAULT_FILENAME;
    }

    private function setFieldLogData(mixed $logData): void {
        $this->logData = $logData;
    }
    private function getFieldLogData(): mixed {
        return $this->logData;
    }

    private function setFieldIsDelimiterNeedle(bool $isDelimiterNeedle): void {
        $this->isDelimiterNeedle = $isDelimiterNeedle;
    }
    private function getFieldIsDelimiterNeedle(): bool {
        return $this->isDelimiterNeedle;
    }
}